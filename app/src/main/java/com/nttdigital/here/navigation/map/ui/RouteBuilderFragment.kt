package com.nttdigital.here.navigation.map.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.nttdigital.here.navigation.R
import kotlinx.android.synthetic.main.fragment_build_route.*

class RouteBuilderFragment : Fragment(R.layout.fragment_build_route) {

    private lateinit var viewModel: RouteViewModel
    private lateinit var adapter: NearbyWaypointsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = activity?.run {
            ViewModelProviders.of(this)[RouteViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        adapter =
            NearbyWaypointsAdapter(ClickListener {
//             TODO add simple array adapter to spinner
                viewModel.setWaypoint(it)
                viewModel.startRoute()
            })

        viewModel.waypointsList.observe(this, Observer {
            adapter.data = it
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        way_points_list.adapter = adapter

        btn_drive.setOnClickListener {
//           TODO
        }
    }

    companion object {
        fun newInstance(): RouteBuilderFragment {
            return RouteBuilderFragment()
        }
    }
}
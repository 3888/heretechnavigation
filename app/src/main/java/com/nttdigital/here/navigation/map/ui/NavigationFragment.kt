package com.nttdigital.here.navigation.map.ui

import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.here.android.mpa.common.*
import com.here.android.mpa.common.PositioningManager.getInstance
import com.here.android.mpa.guidance.NavigationManager
import com.here.android.mpa.guidance.NavigationManager.MapUpdateMode
import com.here.android.mpa.guidance.NavigationManager.NavigationManagerEventListener
import com.here.android.mpa.mapping.*
import com.here.android.mpa.mapping.Map
import com.here.android.mpa.routing.*
import com.here.android.mpa.search.*
import com.nttdigital.here.navigation.MainActivity
import com.nttdigital.here.navigation.R
import com.nttdigital.here.navigation.map.domain.WaypointData
import com.nttdigital.here.navigation.map.ui.RouteViewModel.Companion.NULL_OR_BLANK_API_VALUE
import kotlinx.android.synthetic.main.fragment_map.*
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.lang.ref.WeakReference
import java.util.*

class NavigationFragment : Fragment(), Map.OnTransformListener {

    private val parentActivity get() = activity as MainActivity
    private val nearbyWaypointsList: MutableList<WaypointData> = ArrayList()
    private val mapObjectList: MutableList<MapObject> = ArrayList()

    private lateinit var viewModel: RouteViewModel
    private lateinit var routerBuildRouteFragment: RouteBuilderFragment
    private lateinit var coreRouter: CoreRouter
    private lateinit var route: Route
    private lateinit var routePlan: RoutePlan
    private lateinit var centerCoordinate: Pair<Double, Double>

    private var map: Map? = null
    private var mapRoute: MapRoute? = null
    private var positioningManager: PositioningManager? = null
    private var navigationManager: NavigationManager? = null
    private var locationInfo: Runnable? = null
    private var paused: Boolean = false
    private var transforming: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = activity?.run {
            ViewModelProviders.of(this)[RouteViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        viewModel.waypoint.observe(this, androidx.lifecycle.Observer {
            addWayPoint(it.lattude, it.longitude)
            addMarker(it.lattude, it.longitude)
        })

        viewModel.isRouteCompleted.observe(this, androidx.lifecycle.Observer {
            if (it) calculateRoute()
            hideRouteBuilderFragment()
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initMapFragmentView()

        btn_find_way_point.setOnClickListener {
            openRouteBuilder()
        }
    }

    private fun initMapFragmentView() {
        // This will use external storage to save map cache data, it is also possible to set
        // private app's path
        val path = File(parentActivity.getExternalFilesDir(null), ".here-map-data")
            .absolutePath
        // This method will throw IllegalArgumentException if provided path is not writable
        MapSettings.setDiskCacheRootPath(path)

        val mapFragment =
            this.childFragmentManager.findFragmentById(R.id.heremap) as AndroidXMapFragment?

        val context = ApplicationContext(parentActivity)

        mapFragment?.let { fragment ->
            /**
             * Initialize the AndroidXMapFragment, results will be given via the called back.
             */
            fragment.init(context) { error ->
                when (error) {
                    // If no error returned from map fragment initialization
                    OnEngineInitListener.Error.NONE -> {
                        // Get the map object
                        map = fragment.map

                        if (isGPSGranted()) {
                            getCurrentLocation()
                        }

                        /*
                         * Get the NavigationManager instance.It is responsible for providing voice
                         * and visual instructions while driving and walking
                         */
                        navigationManager = NavigationManager.getInstance()

                        // Configure map
                        map?.run {
                            // TODO
                        }
                    }
                    // If error occurred during engine initialization
                    else -> {
                        val errorMessage =
                            "Error: ${error}, SDK Version: ${Version.getSdkVersion()}"
                        Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_LONG).show()
                        Timber.e(errorMessage)
                    }
                }
            }
        }
    }

    private fun isGPSGranted(): Boolean {
        val locationManager = requireContext()
            .applicationContext
            .getSystemService(Context.LOCATION_SERVICE) as LocationManager

        val isGPSGranted = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if (!isGPSGranted) {
            AlertDialog.Builder(requireContext())
                .setTitle("GPS not found")
                .setMessage("Enable GPS?") // Want to enable?
                .setPositiveButton("Yes") { _, _ -> this.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)) }
                .setNegativeButton("No", null)
                .show()
        }

        return isGPSGranted
    }

    private fun getCurrentLocation() {
        //TODO Important! Check why getCurrentLocation works not correct while restart app
        positioningManager = getInstance()
        positioningManager?.start(PositioningManager.LocationMethod.GPS_NETWORK)
        positioningManager?.addListener(WeakReference(addPositionListener()))

        if (positioningManager!!.start(PositioningManager.LocationMethod.GPS_NETWORK)) run {
            map?.positionIndicator?.setVisible(true)
        } else {
            Toast.makeText(
                requireContext(),
                "PositioningdManager.start: failed, exiting",
                Toast.LENGTH_LONG
            ).show()
            map?.zoomLevel =
                (map?.maxZoomLevel?.plus(map?.minZoomLevel!!))?.div(1)!!
        }
    }

    private fun addPositionListener(): PositioningManager.OnPositionChangedListener =
        object : PositioningManager.OnPositionChangedListener {
            override fun onPositionUpdated(
                locationMethod: PositioningManager.LocationMethod,
                geoPosition: GeoPosition?, isMapMatched: Boolean
            ) {
                if (transforming) {
                    locationInfo = Runnable {
                        onPositionUpdated(locationMethod, geoPosition, isMapMatched)
                    }
                } else {
                    if (geoPosition != null) {
                        updateStartPosition(geoPosition)
                    }
                }
            }

            override fun onPositionFixChanged(
                method: PositioningManager.LocationMethod,
                status: PositioningManager.LocationStatus
            ) {
            }
        }

    private fun initRouteOptions() {
        /* Initialize a CoreRouter */
        coreRouter = CoreRouter()

        /* Initialize a RoutePlan */
        routePlan = RoutePlan()

        /*
         * Initialize a RouteOption. HERE Mobile SDK allow users to define their own parameters for the
         * route calculation,including transport modes,route types and route restrictions etc.Please
         * refer to API doc for full list of APIs
         */
        val routeOptions = RouteOptions()
        /* Other transport modes are also available e.g Pedestrian */
        routeOptions.transportMode = RouteOptions.TransportMode.CAR
        /* Disable highway in this route. */
        routeOptions.setHighwaysAllowed(false)
        /* Calculate the shortest route available. */
        routeOptions.routeType = RouteOptions.Type.SHORTEST
        /* Calculate 1 route. */
        routeOptions.routeCount = 1
        /* Finally set the route option */
        routePlan.routeOptions = routeOptions
    }

    private fun updateStartPosition(geoPosition: GeoPosition) {
        initRouteOptions()

        /*To create route we need two waypoints. Add start waypoint to the route plan */

        centerCoordinate = geoPosition.coordinate.latitude to geoPosition.coordinate.longitude
        //TODO Important! Check why center coordinates is not set
        map?.setCenter(
            GeoCoordinate(centerCoordinate.first, centerCoordinate.second),
            Map.Animation.BOW
        )

        addWayPoint(centerCoordinate.first, centerCoordinate.second)
        btn_find_way_point.visibility = View.VISIBLE

        findNearbyPlaces()
    }

    private fun addWayPoint(latitude: Double, longitude: Double) {
        routePlan.addWaypoint(
            RouteWaypoint(
                GeoCoordinate(latitude, longitude)
            )
        )
    }

    private val discoveryResultPageListener: ResultListener<DiscoveryResultPage> =
        ResultListener<DiscoveryResultPage> { discoveryResultPage, errorCode ->
            if (errorCode == ErrorCode.NONE) {
                /* No error returned,let's handle the results */

                val items = discoveryResultPage?.items

                if (items != null) {
                    for (item in items) {

                        if (item.resultType == DiscoveryResult.ResultType.PLACE) {
                            val placeLink = item as PlaceLink

                            if (placeLink.position != null) {
                                getLocationDetails(
                                    placeLink.position!!.latitude,
                                    placeLink.position!!.longitude,
                                    placeLink.title
                                )
                            }
                        }
                    }

                    viewModel.setWaypointsList(nearbyWaypointsList)
                    progressBar.visibility = View.GONE
                    btn_find_way_point.visibility = View.VISIBLE
                }
            } else {
                Toast.makeText(
                    requireContext(),
                    "ERROR:Discovery search request returned return error code+ $errorCode",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

    private fun findNearbyPlaces() {
        /*
      * Trigger an AroundRequest based on the current map center and the filter for
      * chosen category.Please refer to HERE Android SDK API doc for other supported
      * location parameters and categories.
         */

        val aroundRequest = AroundRequest()
        aroundRequest.setSearchCenter(
            GeoCoordinate(
                centerCoordinate.first,
                centerCoordinate.second
            )
        )

        val filter =
            CategoryFilter()
        filter.add(Category.Global.EAT_DRINK)
        aroundRequest.setCategoryFilter(filter)
        aroundRequest.execute(discoveryResultPageListener)
    }

    private fun getLocationDetails(latitude: Double, longitude: Double, title: String) =
        ReverseGeocodeRequest(GeoCoordinate(latitude, longitude)).execute { location, errorCode ->
            if (errorCode == ErrorCode.NONE) {
                /*
               * From the location object, we retrieve the address and display to the screen.
               * Please refer to HERE Android SDK doc for other supported APIs.
               */

                addPlaceToNearbyWaypointsList(
                    latitude,
                    longitude,
                    title,
                    location?.address
                )

            } else {
                Timber.e("LocationDetails Error $errorCode")
            }
        }

    private fun addPlaceToNearbyWaypointsList(
        latitude: Double?,
        longitude: Double?,
        title: String,
        address: Address?
    ) {
        if (latitude == null) {
            Timber.e("Error latitude is $latitude")
            return
        } else {
            if (longitude == null) {
                Timber.e("Error longitude is $longitude")
                return
            } else {
                val placeAddress = viewModel.convertAddressToString(address)

                if (!placeAddress.contains(NULL_OR_BLANK_API_VALUE)) {
                    nearbyWaypointsList.add(WaypointData(latitude, longitude, title, placeAddress))
                }
            }
        }
    }

    private fun addMarker(latitude: Double, longitude: Double) {
        /*
        * Add a marker for each result of PlaceLink type.For best usability, map can be
        * also adjusted to display all markers.This can be done by merging the bounding
        * box of each result and then zoom the map to the merged one.
        */

        val img = Image()
        try {
            img.setImageResource(R.drawable.marker)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val mapMarker = MapMarker()
        mapMarker.coordinate = GeoCoordinate(latitude, longitude)
        map?.addMapObject(mapMarker)
        mapObjectList.add(mapMarker)
    }

    private fun cleanMap() {
        if (mapObjectList.isNotEmpty()) {
            map?.removeMapObjects(mapObjectList)
            mapObjectList.clear()
        }
    }

    private fun openRouteBuilder() {
        if (parentActivity.supportFragmentManager.fragments.size == 1) addRouteBuilderFragment() else showRouteBuilderFragment()
    }

    private fun addRouteBuilderFragment() {
        routerBuildRouteFragment =
            RouteBuilderFragment.newInstance()

        btn_find_way_point.visibility = View.GONE
        parentActivity.supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.slide_in_top, R.anim.slide_out_bottom)
            .add(R.id.navHostFragment, routerBuildRouteFragment)
            .runOnCommit {
                routerBuildRouteFragment.view?.setOnClickListener {
                    hideRouteBuilderFragment()
                }
            }
            .commit()
    }

    private fun showRouteBuilderFragment() {
        btn_find_way_point.visibility = View.GONE
        parentActivity.supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.slide_in_top, R.anim.slide_out_bottom)
            .show(routerBuildRouteFragment)
            .commit()
    }

    private fun hideRouteBuilderFragment() {
        btn_find_way_point.visibility = View.VISIBLE
        parentActivity.supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.slide_in_top, R.anim.slide_out_bottom)
            .hide(routerBuildRouteFragment)
            .commit()
    }

    /* Creates a route from your location to nearby chosen points */
    private fun calculateRoute() {
        if (routePlan.waypointCount < 2) {
            Toast.makeText(
                requireContext(),
                "To build a route we need at least 1 point selected",
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        /* Trigger the route calculation,results will be called back via the listener */
        coreRouter.calculateRoute(
            routePlan,
            object : Router.Listener<List<RouteResult>, RoutingError> {
                override fun onProgress(i: Int) {
                    /* The calculation progress can be retrieved in this callback. */
                }

                override fun onCalculateRouteFinished(
                    routeResults: List<RouteResult>?,
                    routingError: RoutingError
                ) {
                    /* Calculation is done. Let's handle the result */
                    if (routingError == RoutingError.NONE) {
                        if (routeResults!![0].route != null) {
                            route = routeResults[0].route

                            /* Create a MapRoute so that it can be placed on the map */
                            mapRoute = MapRoute(routeResults[0].route)

                            /* Show the maneuver number on top of the route */
                            mapRoute!!.isManeuverNumberVisible = true

                            /* Add the MapRoute to the map */map?.addMapObject(mapRoute!!)

                            /** We may also want to make sure the map view is orientated properly
                                 * so the entire route can be easily seen.
                                 */
                            val gbb = routeResults[0].route
                                .boundingBox
                            if (gbb != null) {
                                map?.zoomTo(
                                    gbb, Map.Animation.NONE,
                                    Map.MOVE_PRESERVE_ORIENTATION
                                )

                                startNavigation()
                            }
                        } else {
                            Toast.makeText(
                                requireContext(),
                                "Error:route results returned is not valid",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Error:route calculation returned error code: $routingError",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            })
    }

    private fun startNavigation() {
        if (navigationManager != null) {
            /* Configure Navigation manager to launch navigation on current map */
            navigationManager!!.setMap(
                map
            )

            /*
         * Start the turn-by-turn navigation.Please note if the transport mode of the passed-in
         * route is pedestrian, the NavigationManager automatically triggers the guidance which is
         * suitable for walking. Simulation and tracking modes can also be launched at this moment
         * by calling either simulate() or startTracking()
         */

            navigationManager!!.simulate(route, 50) //Simualtion speed is set to 50 m/s
            map?.tilt = 60f
            /*
         * Set the map update mode to ROADVIEW.This will enable the automatic map movement based on
         * the current location.If user gestures are expected during the navigation, it's
         * recommended to set the map update mode to NONE first. Other supported update mode can be
         * found in HERE Mobile SDK for Android (Premium) API doc
         */
            navigationManager!!.mapUpdateMode = MapUpdateMode.ROADVIEW

            /*
         * NavigationManager contains a number of listeners which we can use to monitor the
         * navigation status and getting relevant instructions.In this example, we will add 2
         * listeners for demo purpose,please refer to HERE Android SDK API documentation for details
         */
            addNavigationListeners()
        }
    }

    private fun addNavigationListeners() {
        /*
         * Register a NavigationManagerEventListener to monitor the status change on
         * NavigationManager
         */
        navigationManager!!.addNavigationManagerEventListener(
            WeakReference(
                m_navigationManagerEventListener
            )
        )

        /* Register a PositionListener to monitor the position updates */navigationManager!!.addPositionListener(
            WeakReference(m_positionListener)
        )
    }

    private val m_positionListener: NavigationManager.PositionListener =
        object : NavigationManager.PositionListener() {
            override fun onPositionUpdated(geoPosition: GeoPosition) {
                /* Current position information can be retrieved in this callback */
            }
        }

    private val m_navigationManagerEventListener: NavigationManagerEventListener =
        object : NavigationManagerEventListener() {
            override fun onRunningStateChanged() {
                Toast.makeText(parentActivity, "Running state changed", Toast.LENGTH_SHORT).show()
            }

            override fun onNavigationModeChanged() {
                Toast.makeText(parentActivity, "Navigation mode changed", Toast.LENGTH_SHORT).show()
            }

            override fun onEnded(navigationMode: NavigationManager.NavigationMode) {
                Toast.makeText(
                    parentActivity,
                    "$navigationMode was ended",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onMapUpdateModeChanged(mapUpdateMode: MapUpdateMode) {
                Toast.makeText(
                    parentActivity, "Map update mode is changed to $mapUpdateMode",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onRouteUpdated(route: Route) {
                Toast.makeText(parentActivity, "Route updated", Toast.LENGTH_SHORT).show()
            }

            override fun onCountryInfo(s: String, s1: String) {
                Toast.makeText(
                    parentActivity, "Country info updated from $s to $s1",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

    override fun onPause() {
        positioningManager?.stop()
        super.onPause()
        paused = true
    }

    override fun onResume() {
        super.onResume()
        isGPSGranted()
        paused = false
        positioningManager?.start(PositioningManager.LocationMethod.GPS_NETWORK)
    }

    override fun onDestroy() {
        super.onDestroy()
        map = null

        /* Stop the navigation when app is destroyed */
        if (navigationManager != null) {
            navigationManager!!.stop()
        }
    }

    override fun onMapTransformStart() {
        transforming = true
    }

    override fun onMapTransformEnd(p0: MapState?) {
        transforming = false
        if (locationInfo != null) {
            locationInfo?.run()
            locationInfo = null
        }
    }
}

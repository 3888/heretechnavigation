package com.nttdigital.here.navigation.map.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nttdigital.here.navigation.R
import com.nttdigital.here.navigation.map.domain.WaypointData
import kotlinx.android.synthetic.main.item_waypoint.view.*

class NearbyWaypointsAdapter(val clickListener: ClickListener) :
    RecyclerView.Adapter<NearbyWaypointsAdapter.ViewHolder>() {

    var data = listOf<WaypointData>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]

        holder.bind(clickListener, item, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(
            listener: ClickListener,
            item: WaypointData,
            position: Int
        ) {
            itemView.tv_stop_number_value.text = position.inc().toString()
            itemView.tv_search_result.text = item.placesAddress
            itemView.setOnClickListener {
                listener.onClick(item)
            }
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater
                    .inflate(R.layout.item_waypoint, parent, false)
                return ViewHolder(view)
            }
        }
    }
}

class ClickListener(val clickListener: (data: WaypointData) -> Unit) {
    fun onClick(data: WaypointData) = clickListener(data)
}

package com.nttdigital.here.navigation.map.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.here.android.mpa.search.Address
import com.nttdigital.here.navigation.map.domain.WaypointData

class RouteViewModel : ViewModel() {

    fun convertAddressToString(address: Address?): String {
        return "${if (address?.houseNumber.isNullOrEmpty()) NULL_OR_BLANK_API_VALUE else address?.houseNumber} " +
                "${if (address?.street.isNullOrEmpty()) NULL_OR_BLANK_API_VALUE else address?.street}, " +
                "${if (address?.city.isNullOrEmpty()) NULL_OR_BLANK_API_VALUE else address?.city}, " +
                "${if (address?.countryCode.isNullOrEmpty()) NULL_OR_BLANK_API_VALUE else address?.countryCode}"
    }

    private val _waypointsList = MutableLiveData<List<WaypointData>>()

    val waypointsList: LiveData<List<WaypointData>>
        get() = _waypointsList

    fun setWaypointsList(waypoints: List<WaypointData>) {
        _waypointsList.value = waypoints
    }

    private var _waypoint = MutableLiveData<WaypointData>()

    val waypoint: LiveData<WaypointData>
        get() = _waypoint

    fun setWaypoint(waypoint: WaypointData) {
        _waypoint.value = waypoint
    }

    private var _isRouteCompleted = MutableLiveData<Boolean>()

    val isRouteCompleted: LiveData<Boolean>
        get() = _isRouteCompleted

    fun startRoute() {
        _isRouteCompleted.value = true
    }


    companion object {
        const val NULL_OR_BLANK_API_VALUE = "no information"
    }
}
package com.nttdigital.here.navigation.map.domain

data class WaypointData(
    val lattude: Double,
    val longitude: Double,
    val placeTitle: String,
    val placesAddress: String
)